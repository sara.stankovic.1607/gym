CREATE DATABASE Gym;

-- Person(pin*, password)
CREATE TABLE IF NOT EXISTS Person(
    pin INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    password VARCHAR(15) NOT NULL
);

-- GymUser(pin*, userName, name, surname, endDate)
CREATE TABLE IF NOT EXISTS GymUser(
	pin INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(20) NOT NULL,
	surname VARCHAR(20) NOT NULL,
	endDate DATE,

	FOREIGN KEY (pin) REFERENCES Person(pin)
);

--Manager(pin*)
CREATE TABLE IF NOT EXISTS Manager(
	pin INTEGER NOT NULL PRIMARY KEY,

	FOREIGN KEY (pin) REFERENCES Person(pin)
);


-- History(pin*,visitDate*)
CREATE TABLE IF NOT EXISTS History (
    pin INTEGER NOT NULL,
    visitDate DATE NOT NULL,

    PRIMARY KEY (pin, visitDate),
    FOREIGN KEY (pin) REFERENCES GymUser(pin)
);


-- Coach(pin*, name, surname, birthYear)
CREATE TABLE IF NOT EXISTS Coach(
	pin INTEGER NOT NULL PRIMARY KEY,
    name VARCHAR(20) NOT NULL,
    surname VARCHAR(20) NOT NULL,
    birthYear INTEGER,

    FOREIGN KEY (pin) REFERENCES Person(pin)
);

-- Course(id*, name, description, level)
CREATE TABLE IF NOT EXISTS Course(
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(20) NOT NULL,
	description VARCHAR(150),
	level VARCHAR(12) NOT NULL
		CHECK (level in ('beginner', 'intermediate', 'advanced'))
);

-- Class(id*, courseId, coachPin, dateC, startTime)
CREATE TABLE IF NOT EXISTS Class(
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	courseId INTEGER NOT NULL,
	coachPin INTEGER NOT NULL,
	dateC DATE NOT NULL,
	startTime INTEGER NOT NULL
		check (startTime between 0 and 23),

	FOREIGN KEY (courseId) REFERENCES Course (id),
	FOREIGN KEY (coachPin) REFERENCES Coach (pin)
);




