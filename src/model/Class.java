package model;

import javax.persistence.*;
import java.util.Date;

// Class(id*, courseId, coachPin, dateC, startTime)

@Entity
@Table(name = "Class")
public class Class {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "courseId")
    private Course course;

    @ManyToOne
    @JoinColumn(name = "coachPin")
    private Coach coach;

    @Column(name = "dateC", nullable = false)
    private Date dateC;

    @Column(name = "startTime", nullable = false)
    private int startTime;

    public Class(){}

    public Class(int id, Course course, Coach coach, Date dateC, int startTime) {
        this.id = id;
        this.course = course;
        this.coach = coach;
        this.dateC = dateC;
        this.startTime = startTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public Date getDateC() {
        return dateC;
    }

    public void setDateC(Date dateC) {
        this.dateC = dateC;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }
}

