package model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "History")
public class History implements Comparable{

    @EmbeddedId
    private HistoryId id;

    @ManyToOne
    @JoinColumn(name = "pin", insertable = false, updatable = false)
    private GymUser gymUser;

    @Column(name = "visitDate", insertable = false,  updatable = false)
    private Date visitDate;

    public History() {}

    public History(GymUser gymUser, Date visitDate) {
        this.id = new HistoryId(gymUser.getPin(), visitDate);
        this.gymUser = gymUser;
        this.visitDate = visitDate;
    }

    public GymUser getGymUser() {
        return gymUser;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public HistoryId getId() {
        return id;
    }

    public void setId(HistoryId id) {
        this.id = id;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public void setGymUser(GymUser gymUser) {
        this.gymUser = gymUser;
    }

    @Override
    public int compareTo(Object o) {
        History h = (History)o;
        return this.getVisitDate().compareTo(h.getVisitDate());
    }
}

