package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

// Course(id*, name, description, level)

@Entity
@Table(name = "Course")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "level", nullable = false)
    private String level;

    @OneToMany(mappedBy = "course")
    private List<Class> classes = new ArrayList<>();

    public Course() {}

    public Course(int id, String name, String description, String level) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.level = level;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }


    @Override
    public String toString() {
        return name + "(" + level + ")";
    }

    public void setClasses(List<Class> classes) {
        this.classes = classes;
    }

    public List<Class> getClasses() {
        return classes;
    }
}

