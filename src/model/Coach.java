package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

// Coach(pin*, name, surname, birthYear)

@Entity
@Table(name = "Coach")
@PrimaryKeyJoinColumn(name = "pin")
public class Coach extends Person {

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "birthYear")
    private int birthYear;

    @OneToMany(mappedBy = "coach")
    private List<Class> classes;

    public Coach(){
    }

    public Coach(int pin, String password, String name, String surname, int birthYear) {
        super(pin, password);
        this.name = name;
        this.surname = surname;
        this.birthYear = birthYear;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public List<Class> getClasses() {
        return classes;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    @Override
    public String toString() {
        return name + " " + surname + ", " + birthYear + ".";
    }

    public void setClasses(List<Class> classes) {
        this.classes = classes;
    }
}


