package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Embeddable
public class HistoryId implements Serializable {

    private int pin;
    private Date visitDate;

    public HistoryId() {
    }

    public HistoryId(int pin, Date visitDate) {
        this.pin = pin;
        this.visitDate = visitDate;
    }

    public int getPin() {
        return pin;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HistoryId historyId = (HistoryId) o;
        return pin == historyId.pin &&
                Objects.equals(visitDate, historyId.visitDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pin, visitDate);
    }
}

