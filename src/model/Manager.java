package model;

import javax.persistence.*;

@Entity
@Table(name = "Manager")
@PrimaryKeyJoinColumn(name = "pin")
public class Manager extends Person {
    public Manager(){}

    public Manager(int pin, String password){
        super(pin, password);
    }
}
