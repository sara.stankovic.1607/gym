package model;

import javax.persistence.*;

// Person(pin*, password)

@Entity
@Table(name = "Person")
@Inheritance(strategy = InheritanceType.JOINED)
public class Person {

    @Id
    @Column(name = "pin")
    @GeneratedValue(strategy = GenerationType.IDENTITY) //GenerationType.IDENTITY)
    private int pin;

    @Column(name = "password")
    private String password;


    public Person(){}

    public Person(int pin, String password){
        this.pin = pin;
        this.password = password;
    }

    public int getPin() {
        return pin;
    }

    public String getPassword() {
        return password;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
