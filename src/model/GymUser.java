package model;

import app.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.persistence.*;
import java.lang.reflect.Array;
import java.util.*;

// GymUser(pin*, userName, name, surname, endDate)

@Entity
@Table(name = "GymUser")
@PrimaryKeyJoinColumn(name = "pin")
public class GymUser extends Person {

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Temporal(TemporalType.DATE)
    @Column(name = "endDate", nullable = true)
    private Date endDate;

    @OneToMany(mappedBy = "gymUser", fetch = FetchType.EAGER)
    private List<History> historyList = new ArrayList<>();


    public GymUser(){
    }

    public GymUser(int pin, String password, String name, String surname, Date endDate) {
        super(pin, password);
        this.name = name;
        this.surname = surname;
        this.endDate = endDate;
    }


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Date getEndDate() {
        return endDate;
    }

    public List<History> getHistoryList() {
        return historyList;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isActiv() {
        return endDate != null;
    }

    public void setHistoryList(List<History> historyList){
        this.historyList = historyList;
    }

    public static int getMaxPin() {
        Session session = HibernateUtil.getSessionFactory().openSession();

        String hql = "SELECT MAX(pin) FROM Person";
        int maxPin = session.createQuery(hql, Integer.class).uniqueResult();

        session.close();

        return maxPin;
    }
}

