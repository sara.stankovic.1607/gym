package scenes;

import app.HibernateUtil;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.*;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Date;

public class ManagerScene extends Scene {
    public ManagerScene(Manager manager) {
        super(createScene(manager), 600, 400);
    }

    private static Parent createScene(Manager manager) {

        VBox root = new VBox(20);

        Button btAddNewUser = new Button("add new user");
        btAddNewUser.setOnAction(e -> addNewUser());

        Button btAddCheckIn = new Button("add check in for user");
        btAddCheckIn.setOnAction(e -> addCheckIn());

        root.getChildren().addAll(btAddNewUser, btAddCheckIn);
        return root;
    }

    private static void addCheckIn() {
        Stage stage = new Stage();
        stage.setTitle("check in");

        VBox root = new VBox(20);
        TextField tfPin = new TextField();
        Button btSave = new Button("save");

        btSave.setOnAction(e -> {

            // TODO: check

            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = null;

            try{
                transaction = session.beginTransaction();

                GymUser user = session.get(GymUser.class, Integer.valueOf(tfPin.getText()));

                if(user != null){
                    History history = new History(user, new Date());
                    session.save(history);
                }

                transaction.commit();

            } catch(Exception ex){
                ex.printStackTrace();
                if(transaction != null)
                    transaction.rollback();;
            } finally {
                session.close();
                stage.close();
            }
        });

        root.getChildren().addAll(new Label("user's pin:"), tfPin, btSave);

        Scene scene = new Scene(root, 200, 200);
        stage.setScene(scene);
        stage.showAndWait();
    }

    private static void addNewUser() {
        Stage stage = new Stage();
        stage.setTitle("Add new gym user");

        VBox vbox = new VBox(10);
        vbox.setPadding(new Insets(20));

        Label lPin = new Label("Pin:");
        TextField tfPin = new TextField();
        tfPin.setText("" + (GymUser.getMaxPin()+1));
        tfPin.setEditable(false);

        Label lName = new Label("Name:");
        TextField tfName = new TextField();

        Label lSurname = new Label("Surname:");
        TextField tfSurname = new TextField();

        Label lPassword = new Label("Password:");
        PasswordField pfPassword = new PasswordField();

        Label lPasswordAgain = new Label("Password:");
        PasswordField pfPasswordAgain = new PasswordField();

        Button btSaveAndExit = new Button("save and exit");
        btSaveAndExit.setOnAction(e -> {
            // TODO: check
            //Person person = new Person();
            //person.setPassword(pfPassword.getText());

            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = null;

            try{
                transaction = session.beginTransaction();
                //session.save(person);

                GymUser user = new GymUser();
                user.setPassword(pfPassword.getText());
                user.setName(tfName.getText());
                user.setSurname(tfSurname.getText());

                session.save(user);

                transaction.commit();

            } catch (Exception ex) {
                ex.printStackTrace();
                if(transaction != null)
                    transaction.rollback();
            } finally {
                session.close();
            }

            stage.close();
        });

        vbox.getChildren().addAll(lPin, tfPin,
                                lName, tfName,
                                lSurname, tfSurname,
                                lPassword, pfPassword,
                                lPasswordAgain, pfPasswordAgain,
                                btSaveAndExit);

        Scene scene = new Scene(vbox, 400, 400);
        stage.setScene(scene);
        stage.showAndWait();
    }
}
