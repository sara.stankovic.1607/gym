package scenes;

import app.Main;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.*;

public class MainScene extends Scene {


    public MainScene() {
        super(createScene(), 600, 400);
    }

    private static HBox createScene() {
        //
        HBox root = new HBox(20);

        Button btGymUser = new Button();
        Button btManager = new Button();

        root.getChildren().addAll(btGymUser, btManager);
        //

        createDesign(root, btGymUser, btManager);

        btGymUser.setOnAction(e -> { ;
            Main.changeScene(new UserLoginScene());
        });

        btManager.setOnAction(e -> {
            Main.changeScene(new ManagerLoginScene());
        });

        return root;
    }

    private static void createDesign(HBox root, Button btGymUser, Button btManager) {
        root.setAlignment(Pos.BOTTOM_LEFT);

        btGymUser.setBackground(new Background(new BackgroundImage(
                new Image("gymUserIcon.png"),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(100, 100, false, false, true, false)
        )));
        btGymUser.setPrefSize(120, 120);
        btGymUser.setBorder(new Border(new BorderStroke(javafx.scene.paint.Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(20), new BorderWidths(2))));


        btManager.setBackground(new Background(new BackgroundImage(
                new Image("managerIcon.png"),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(100, 100, false, false, true, false)
        )));
        btManager.setPrefSize(120, 120);
        btManager.setBorder(new Border(new BorderStroke(javafx.scene.paint.Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(20), new BorderWidths(2))));


        root.setBackground(new Background(new BackgroundImage(
                new Image("mainSceneBackground.jpg"),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                BackgroundSize.DEFAULT
        )));
    }
}
