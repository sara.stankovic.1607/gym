package scenes;

import app.HibernateUtil;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.*;
import model.Class;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.text.SimpleDateFormat;
import java.util.*;


public class GymUserScene extends Scene {

    public GymUserScene(GymUser gymUser) {
        super(createScene(gymUser), 600, 400);
    }

    private static Parent createScene(GymUser gymUser) {

        VBox root = new VBox(20);

        HBox hb1 = new HBox(20);
        Button btMyData = new Button("my\ndata");
        Button btMyMembership = new Button("my\nmembership");
        Button btHistory = new Button("my\nhistory");
        hb1.getChildren().addAll(btMyData, btMyMembership, btHistory);


        HBox hb2 = new HBox(20);
        Button btCoaches = new Button("our\ntrainers");
        Button btCourses = new Button("our\ncourses");
        Button btBooking = new Button("booking\nclasses");
        hb2.getChildren().addAll(btCoaches, btCourses, btBooking);

        root.getChildren().addAll(hb1, hb2);

        root.setAlignment(Pos.CENTER);
        root.setPadding(new Insets(20));

        ////////
        btMyData.setPrefSize(100, 100);
        btMyMembership.setPrefSize(100, 100);
        btHistory.setPrefSize(100, 100);
        hb1.setMaxSize(340, 100);

        btCoaches.setPrefSize(100, 100);
        btCourses.setPrefSize(100, 100);
        btBooking.setPrefSize(100, 100);
        hb2.setMaxSize(340, 100);

        root.setBackground(new Background(new BackgroundImage(
                new Image("gymUserSceneBackground.jpeg"),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(1.0, 1.0, true, true, false, false)
        )));

        ///

        btMyData.setOnAction(e -> {
            createStageForData(gymUser);
        });

        btMyMembership.setOnAction(e -> {
            createAlertMembership(gymUser);
        });

        btHistory.setOnAction(e -> {
            createAlertHistory(gymUser);
        });

        btCoaches.setOnAction(e -> {
            listAllCoaches();
        });

        btCourses.setOnAction(e -> {
            listAllCourses();
        });

        btBooking.setOnAction(e -> {
            createBookingStage(gymUser);
        });

        return root;
    }

    private static void createBookingStage(GymUser gymUser) {
        Stage stage = new Stage();

        VBox root = new VBox(10);
        Scene scene = new Scene(root);

        Button btMyClasses = new Button("my classes");
        btMyClasses.setOnAction(e -> {
            // TODO: create Booking table and list all booked classes
        });

        root.getChildren().add(btMyClasses);

        // ------------------------------------
        VBox vb = new VBox(15);
        Label lClasses = new Label("classes:");
        vb.getChildren().add(lClasses);

        // TODO: refactor
        // catching all classes in the next 7 days
        Session session = HibernateUtil.getSessionFactory().openSession();

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)+6);
        Date myDate = cal.getTime();

        try{

            String hql = "FROM Class WHERE dateC BETWEEN :startDate and :endDate";
            Query<Class> query = session.createQuery(hql, Class.class);
            query.setParameter("startDate", new Date());
            query.setParameter("endDate", myDate);

            List<Class> classes = query.getResultList();

            for(Class c : classes){
                Coach coach = c.getCoach();
                Course course = c.getCourse();

                Button btBook = new Button("book");

                btBook.setOnAction(e -> {
                    // TODO
                });

                VBox vb1 = new VBox();
                vb1.getChildren().addAll(
                        new Text(course.getName() + "\n" +
                                coach.getName() + " " + coach.getSurname() + "\n" +
                                (new SimpleDateFormat("dd.MM.yyyy.").format(c.getDateC())) +
                                " " + c.getStartTime() + "h"),
                        btBook
                );

                vb1.setStyle(
                        "-fx-border-color: black; " +
                        "-fx-border-width: 1px 1px 1px 1px;"
                );

                vb.getChildren().addAll(vb1);
            }

            vb.setStyle(
                    "-fx-border-color: black; " +
                    "-fx-border-width: 1px 1px 1px 1px;"
            );

        } catch (Exception ex){
            ex.printStackTrace();
        } finally {
            session.close();
        }

        root.getChildren().add(vb);
        // ------------------------------------

        stage.setScene(scene);
        stage.showAndWait();
    }

    private static void listAllCourses() {
        // TODO: show description
        Session session = HibernateUtil.getSessionFactory().openSession();

        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle("Our courses");

        List<Course> courses = session.createQuery("FROM Course", Course.class).getResultList();

        ListView<Course> listView = new ListView<>();
        for(Course c : courses) {
            listView.getItems().add(c);
        }

        session.close();

        GridPane gridPane = new GridPane();
        gridPane.setPrefSize(200, 200);
        gridPane.add(listView, 0, 0);
        dialog.getDialogPane().setContent(gridPane);

        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);

        dialog.showAndWait();
    }

    private static void listAllCoaches() {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle("Our coaches");
        //dialog.setHeaderText("Coaches:");

        List<Coach> coaches = session.createQuery("FROM Coach", Coach.class).getResultList();

        //ListView<String> listView = new ListView<>();
        ListView<Button> listView = new ListView<>();
        for(Coach c : coaches){
            Button b = new Button(c.toString());
            b.setOnAction(e -> {
                // TODO: show all course for that trainer
            });
            listView.getItems().add(new Button(c.toString()));
        }


        session.close();

        GridPane gridPane = new GridPane();
        gridPane.setPrefSize(200, 200);
        gridPane.add(listView, 0, 0);
        dialog.getDialogPane().setContent(gridPane);

        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);

        dialog.showAndWait();
    }

    private static void createAlertHistory(GymUser gymUser) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setGraphic(null);

        Session session = HibernateUtil.getSessionFactory().openSession();

        StringBuilder stringBuilder = new StringBuilder();

        List<History> histories = gymUser.getHistoryList();
        Collections.sort(histories);

        for(History h : histories){
            stringBuilder.append(h.getVisitDate()).append("\n");
        }

        session.close();

        alert.setContentText(stringBuilder.toString());
        alert.setTitle("Check in history");

        alert.showAndWait();
    }

    private static void createStageForData(GymUser user) {
        Stage stage = new Stage();

        VBox vb = new VBox(20);

        String name = user.getName();
        String surname = user.getSurname();

        TextField tfName = new TextField(name);
        vb.getChildren().addAll(new Label("name"), tfName);

        TextField tfSurname = new TextField(surname);
        vb.getChildren().addAll(new Label("surname"), tfSurname);

        // TODO: change password

        Button btSaveExit = new Button("save and exit");
        vb.getChildren().add(btSaveExit);

        btSaveExit.setOnAction(e -> {
            String newName = tfName.getText().trim();
            String newSurname = tfSurname.getText().trim();

            if(!newName.equals(name) || !newSurname.equals(surname)){

                Transaction transaction = null;
                Session session = HibernateUtil.getSessionFactory().openSession();

                try{
                    transaction = session.beginTransaction();

                    user.setName(newName);
                    user.setSurname(newSurname);
                    session.update(user);

                    transaction.commit();
                } catch (Exception ex) {
                    if (transaction != null) {
                        transaction.rollback();
                    }
                } finally {
                    session.close();
                }

            }

            stage.close();
        });

        /*TODO: stage.setOnCloseRequest(e -> {

        });*/

        Scene scene = new Scene(vb);
        stage.setScene(scene);
        stage.showAndWait();
    }

    private static void createAlertMembership(GymUser gymUser) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setGraphic(null);

        alert.setContentText(gymUser.isActiv() ? "ACTIVE MEMBERSHIP\nexpires on " + gymUser.getEndDate() : "INACTIVE MEMBERSHIP");
        alert.setTitle("Membership");

        alert.showAndWait();
    }

}
