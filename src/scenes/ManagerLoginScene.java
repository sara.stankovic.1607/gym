package scenes;

import app.HibernateUtil;
import app.Main;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.TextAlignment;
import model.GymUser;
import model.Manager;
import org.hibernate.Session;

public class ManagerLoginScene extends Scene {
    public ManagerLoginScene(){
        super(createScene(), 600, 400);
    }

    private static Parent createScene() {

        // 1.
        Label lError = new Label();
        lError.setTextFill(Paint.valueOf("red"));
        lError.setStyle("-fx-font-size: 15px; -fx-font-color: red; -fx-font-weight: bold;");
        lError.setPrefWidth(250);
        lError.setPrefHeight(20);
        lError.setTextAlignment(TextAlignment.CENTER);

        // 2.
        HBox hb1 = new HBox(10);
        Label l1 = new Label("  pin");
        TextField tfPin = new TextField();
        l1.setPrefSize(80, 30);
        tfPin.setPrefSize(160, 30);
        hb1.setPrefSize(250, 30);
        hb1.getChildren().addAll(l1, tfPin);

        // 3.
        HBox hb2 = new HBox(10);
        Label l2 = new Label("  password");
        PasswordField pfPassword = new PasswordField();
        l2.setPrefSize(80, 30);
        pfPassword.setPrefSize(160, 30);
        hb2.setPrefSize(250, 30);
        hb2.getChildren().addAll(l2, pfPassword);

        // 4.
        Button btLogIn = new Button("log in");
        btLogIn.setPrefSize(100, 30);

        btLogIn.setOnAction(eventHandler -> {
            login(lError, tfPin, pfPassword);
        });

        //
        VBox vb = new VBox(20);
        vb.getChildren().addAll(lError, hb1, hb2, btLogIn);
        vb.setMaxSize(270,190);
        vb.setPadding(new Insets(5));

        StackPane root = new StackPane(vb);
        root.setPadding(new Insets(20));

        root.setStyle("-fx-background-color: lightblue;");

        return root;
    }

    private static void login(Label lError, TextField tfPin, PasswordField pfPassword) {
        lError.setText("");
        int pin;
        String password = pfPassword.getText();

        try {
            pin = Integer.parseInt(tfPin.getText().trim());
        } catch(Exception e){
            lError.setText("Enter a number in pin field");
            return;
        }

        if(password.equals("")){
            lError.setText("Enter password");
            return;
        }

        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Manager manager = session.get(Manager.class, pin);
            if(manager == null || !manager.getPassword().equals(password))
                lError.setText("Wrong password or pin");
            else{
                Main.changeScene(new ManagerScene(manager));
            }
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
