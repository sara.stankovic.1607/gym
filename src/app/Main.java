package app;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import scenes.MainScene;

public class Main extends Application {

    private static Stage stage;

    public static void main(String[] args) {
        launch(args);

        HibernateUtil.getSessionFactory().close();
    }

    @Override
    public void start(Stage stage) throws Exception {

        this.stage = stage;

        MainScene mainScene = new MainScene();

        stage.setTitle("GYM");
        stage.setScene(mainScene);
        stage.show();
    }

    public static void changeScene(Scene scene){
        stage.setScene(scene);
        //stage.show();
    }
}
