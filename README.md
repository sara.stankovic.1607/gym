# Gym Application

The Gym Application is designed to facilitate the management and usage of a gym facility. 
Users can log in as either gym members or managers, each with their respective sets of functionalities.


## Features

### Gym Member

Upon logging in as a gym member, users have access to the following options:  

**Profile Management**: Users can update their personal information.  
**Membership Overview**: View details of their membership.   
**Gym Usage History**: Access a log of their gym usage sessions.  
**Trainers List**: Browse a list of available trainers.  
**Schedule Training**: Book sessions for desired courses.


### Gym Manager

Gym managers have access to additional administrative functionalities:  

**Check-In Management**: Record gym member check-ins.  
**Add New Members**: Enroll new gym members.  
**Membership Management**: Modify membership details.  
**Trainer Management**: Add or update trainer information.  


## Technologies Used

Java (Hibernate, JavaFx), MySQL
